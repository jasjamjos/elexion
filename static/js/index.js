const menuTarget = document.querySelector('[data-menu-toggle]');
const menu = document.querySelector('[data-menu]');

menuTarget.addEventListener('click', (event) => {
  event.preventDefault();
  menuTarget.lastElementChild.classList.toggle('text-white')
  menuTarget.lastElementChild.classList.toggle('text-brandb')
  menu.classList.toggle('hidden');
});

$(document).ready(() => {
  $('.candidate-carousel').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    mobileFirst: true,
    slidesToShow: 5,
    pauseOnDotsHover: true,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1366,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 638,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 300,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      }
    ]
  })
});