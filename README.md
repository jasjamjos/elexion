# Elexion

Election and polling app

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Things you need to run elexion

```
Python 3.7
```

### Installing

Steps on how to run elexion after cloning the repo

```
-Create and enter the virtual environment
virtualenv -p python3.7 env
source env/bin/activate

-Install requirements.txt
pip install -r requirements.txt

-Migrate
python manage.py makemigrations app_*
python manage.py migrate

-Run server
python manage.py runserver
```

