from django.urls import path
from django.contrib import admin

from app_company.views import landing_page

urlpatterns = [
    path('', landing_page, name='landing_page'),
    path('admin/', admin.site.urls),
]
